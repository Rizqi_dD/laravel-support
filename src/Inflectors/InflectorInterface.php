<?php

namespace CyberExploits\Support\Inflectors;


interface InflectorInterface {

	public function plural($word);

    public function singular($word);

} 
